public with sharing class QueryBuilder {
    public QueryBuilder() {

    }

    public static String buildObjectQuery (String objectName) {
         // Initialize setup variables
        String query = 'SELECT ';

        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        System.Debug('Size of fields = ' + fields.size());
        // Grab the fields from the describe method and append them to the queryString one by one.
        for(String s : fields.keySet()) {
            
            if(s !='clonesourceid'){
                query += '' + s + ',';
           }   
        }
        
        // Manually add related object's fields that are needed.
        // query += 'Account.Name,'; // modify as needed
        
        // Strip off the last comma if it exists.
        query = query.removeEnd(',');
        // Add FROM statement
        query += ' FROM ' + objectName;
        
        return query;
    }
    public static String buildObjectQuery (String objectName, String whereFilter) {
        String query = buildObjectQuery(objectName);
        
        // Add on a WHERE/ORDER/LIMIT statement as needed
        query += ' ' + whereFilter;
        return query;
    }

    public static SObject getObjectById(String objectName, String id) {
        
        List<SObject> results;
        try {
                String whereFilter = ' WHERE id = \'' + id + '\''; 
                results = database.query(buildObjectQuery(objectName, whereFilter));
            	System.debug('-----' + results.size());
        } catch (QueryException e){
            	System.debug(e);
                //perform exception handling
        }
        return results[0];
    }
}