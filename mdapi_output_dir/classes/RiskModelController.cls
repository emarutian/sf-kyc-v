public class RiskModelController {
    @AuraEnabled
    public static Boolean runAcceleratedAssessments(String accountId) {
      	/* List<Risk_Assessment__c> results = [SELECT Score__c, Rating__c FROM Risk_Assessment__c WHERE CaseStatus__c = 'Open' and CaseReason__c = 'High Score' Limit 14];
        for (Risk_Assessment__c r: results) {
            r.CaseReason__c = 'Major Score Decrease';
          //  r.Score__c = 80 + Integer.valueof((Math.random() * 20));
        }
        update results; */
       //	return true;
       RiskModel.runAcceleratedAssessments(1);
       return true;
    }
    @AuraEnabled
    public static Boolean runDueAssessments(String accountId) {
      	RiskModel.runDueAssessments();
       	return true;
       //return RiskModel.assessAccount(accountId, 'Manual', '');
    }
    @AuraEnabled
    public static Boolean massDelete(String msg) {
        List<Risk_Factor_Assignment__c> r = [select id from Risk_Factor_Assignment__c];
        List<Risk_Assessment__c> ra = [select id from Risk_Assessment__c];
        delete r;
        delete ra;
      //	RiskModel.massDeleteRecords('select id from Risk_Factor_Assignment__c');
       	return true;
       //return RiskModel.assessAccount(accountId, 'Manual', '');
    }
    @AuraEnabled(cacheable=true)
    public static Risk_Assessment__c getRiskAssessment( String id) {
        return [select Score__c, Rating__c, NextReviewDate__c from Risk_Assessment__c where Account__c =:id order by CreatedDate desc limit 1];
    }
}