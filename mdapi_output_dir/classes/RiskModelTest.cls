@isTest
public with sharing class RiskModelTest {
    
    @testSetup
    static void setup() {
        Risk_Model_Configuration__c activeConfig = new Risk_Model_Configuration__c();
        activeConfig.Name = 						'Created by System on ' + Date.today();
        activeConfig.Activated__c = 				true;
        activeConfig.IDVDefaultScore__c = 			100;
        activeConfig.AccountOpenRange__c = 			7;
        activeConfig.DeploymentInitiationRange__c = 365;
        activeConfig.AssessRelatedParties__c = 		true;
        activeConfig.AssessControllingRolesOnly__c= true;
        activeConfig.ReviewPeriodLowRisk__c = 		365 * 3;
        activeConfig.ReviewPeriodMediumRisk__c = 	365 * 2;
        activeConfig.ReviewPeriodHighRisk__c = 		356;
        activeConfig.RuleBasedAssessment__c = 		true;
        activeConfig.UserReviewFlag__c 	= 			true;
        activeConfig.LegacyCustomerStartDate__c  = 	Date.newInstance(2005, 1, 1);
        activeConfig.RescoreOnRiskModelChange__c = 	false;
        activeConfig.HighRatingStartsWith__c    = 	60;
        activeConfig.MediumRatingStartsWith__c  = 	40;
        activeConfig.MajorScoreIncreaseAlert__c =	40;
        activeConfig.MajorScoreDecreaseAlert__c =	40;
        activeConfig.HighScoreAlert__c			=	60;
        activeConfig.AlgorithmType__c			=	'Dynamic Weight Allocation';
        insert activeConfig;
    }
    static testMethod void validateGetRating () {

        System.debug('>> Medium Score starts with: ' + RiskModelConfig.getInstance().MEDIUM_RATING_STARTS_WITH);
        System.assertEquals('Low', RiskModel.getRating(0));
        System.assertEquals('Low', RiskModel.getRating(39));
        System.assertEquals('Medium', RiskModel.getRating(41));
        System.assertEquals('High', RiskModel.getRating(61));
        System.assertEquals('High', RiskModel.getRating(120));
        
        
    }
    static testMethod void validateGetNextReviewDate () {
        Date d = Date.today();
        System.assertEquals(
                    d + RiskModelConfig.getInstance().REVIEW_PERIOD_HIGH_RISK, 
                    RiskModel.getNextReviewDate('High'));
        System.assertEquals(
                    d + RiskModelConfig.getInstance().REVIEW_PERIOD_MEDIUM_RISK, 
                    RiskModel.getNextReviewDate('Medium'));
        System.assertEquals(
                    d + RiskModelConfig.getInstance().REVIEW_PERIOD_LOW_RISK, 
                    RiskModel.getNextReviewDate('Low'));       
    }

    static testMethod void validateGetTotalWeight () {
        List<Risk_Factor__c> rfs = new List<Risk_Factor__c>();
        rfs.add(new Risk_Factor__c(Weight__c = 10));
        rfs.add(new Risk_Factor__c(Weight__c = 20));

        System.assertEquals(30, RiskModel.getTotalWeight(rfs));      
    }

    static testMethod void validateGetApplicableRiskFactors() {
        List<Risk_Factor__c> rfs = new List<Risk_Factor__c>();
        rfs.add(new Risk_Factor__c(Activated__c = true, CustomerType__c = 'Individual'));
        rfs.add(new Risk_Factor__c(Activated__c = true, CustomerType__c = 'Individual;Legal Entity'));
        rfs.add(new Risk_Factor__c(Activated__c = false, CustomerType__c = 'Individual;Legal Entity'));
        insert rfs;
        System.assertEquals(2, RiskModel.getApplicableRiskFactors('Individual').size());   
    }

    static testMethod void validateRiskFactorAssignment() {
        Risk_Factor__c rf = new Risk_Factor__c(
            Activated__c = true,
            Name = 'Account Purpose',
            Weight__c =5,
            CustomerType__c = 'Individual'
            );
        insert rf;
        
        Risk_Factor_Value__c rfv = new Risk_Factor_Value__c(
            Factor__c = 'Account Purpose',
            Value__c = 'Online',
            Score__c = 4
            );
        insert rfv;

        Risk_Factor_Assignment__c rfa = RiskModel.getRiskAssignment(rf, 'Online', 10);

        System.assertEquals('Account Purpose', rfa.Name);
        System.assertEquals('Online', rfa.Value__c);
        System.assertEquals(rf.Weight__c, rfa.Weight__c);
        System.assertEquals(rfv.Score__c, rfa.Score__c);
        //Given'Dynamic Weight Allocation' per setup 5/10 = 50% where 5 is initial weight and 10 is the total weight
        System.assertEquals(50, rfa.Assigned_Weight__c);
        System.assertEquals(4*50/100, rfa.Assigned_Score__c);

        //Dummy Risk Factor...
         Risk_Factor_Value__c rfvDummy = new Risk_Factor_Value__c(
            Factor__c = 'Dummy',
            Value__c = 'Online',
            Score__c = 4
            );
        insert rfvDummy;

        Risk_Factor_Assignment__c rfaDummy = RiskModel.getRiskAssignment(rf, 'Online', 10);

        System.assertEquals('Dummy', rfaDummy.Name);
        System.assertEquals('Not Found - Online', rfaDummy.Value__c);
        System.assertEquals(rf.Weight__c, rfaDummy.Weight__c);
        System.assertEquals(100, rfa.Score__c);
        //Given'Dynamic Weight Allocation' per setup 5/10 = 50% where 5 is initial weight and 10 is the total weight
        System.assertEquals(50, rfa.Assigned_Weight__c);
        System.assertEquals(4*50/100, rfa.Assigned_Score__c);




    }

    //assessAccount
    static testMethod void validateProcessRiskModelChanges () {
        //assessAccount(Account account, String reason, String msg)
        Account a = new Account();
        a.BillingCity = 'New York';
        a.Name ='Test Billy';

        List<Risk_Factor__c> rfs = new List<Risk_Factor__c>();
        rfs.add(new Risk_Factor__c(Weight__c = 10));
        rfs.add(new Risk_Factor__c(Weight__c = 20));
        System.assertEquals(30, RiskModel.getTotalWeight(rfs));

        
    }
}