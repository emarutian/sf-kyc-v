global class AcceleratedKYCReviewScheduler implements Schedulable {
    //simple call to the method 
    global void execute(SchedulableContext ctx) {
		RiskModel.runAcceleratedAssessments(1);
    }
}