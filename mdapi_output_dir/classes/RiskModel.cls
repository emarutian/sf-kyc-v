global without sharing class RiskModel {
   
    public static Boolean assessAccount(Account account, String reason, String msg) {
        Boolean toReload = true;
        String accountId = account.id;
        if(accountId != null) {
            Decimal previousScore = 0;
            
            /* Get previous assessment(s) if exist to 
             * 	(A) get the previous score and 
             * 	(B) archive them if they show as current
			*/
            List<Risk_Assessment__c> previous = [SELECT Score__c, Current__c FROM Risk_Assessment__c 
                                                 WHERE Account__c =:accountId AND Current__c = true 
                                                 ORDER BY CreatedDate desc];
        	if(previous.size() > 0){
                previousScore = previous[0].Score__c;
                for (Risk_Assessment__c p : previous) {
                    p.Current__c = false;
                }
                update previous;
            }
            
            Risk_Assessment__c riskAssessment = new Risk_Assessment__c();
            riskAssessment.Account__c = accountId;
            riskAssessment.Current__c = true;
            riskAssessment.PreviousScore__c = previousScore;
            riskAssessment.PreviousRating__c= getRating(previousScore);
            riskAssessment.Type__c = reason;
            riskAssessment.Details__c = msg;
            riskAssessment.RiskModelConfiguration__c = RiskModelConfig.getInstance().Id;
           	
            insert riskAssessment;
            
            /* Calculate the new score */
            List<Risk_Factor__c> riskFactors = getApplicableRiskFactors(account.CustomerType__c); 
            String customerType = (String)account.get('CustomerType__c');
            List<Risk_Factor_Assignment__c> rfas = new List<Risk_Factor_Assignment__c>();
          
            Decimal score = 0;
            Decimal totalWeight = getTotalWeight(riskFactors);
            for (Risk_Factor__c rf : riskFactors) {
                String value = (String)account.get(rf.Source__c);
                System.debug('>>> Total risk factors size: ' + riskFactors.size());
                Risk_Factor_Assignment__c rfa = getRiskAssignment(rf, value, totalWeight);
                rfa.Risk_Assessment__c = riskAssessment.id;
                score = score + rfa.Assigned_Score__c;
                rfas.add(rfa);
            }
            riskAssessment.Score__c = score;

            if (score > RiskModelConfig.getInstance().HIGH_SCORE_ALERT) {
                riskAssessment.CaseStatus__c = 'Open';
                riskAssessment.CaseReason__c = 'High Score';        
            }
            if (score - previousScore > RiskModelConfig.getInstance().MAJOR_SCORE_INCREASE_ALERT) {
                riskAssessment.CaseStatus__c = 'Open';
                riskAssessment.CaseReason__c = 'Major Score Increase';        
            }
            if (previousScore - score > RiskModelConfig.getInstance().MAJOR_SCORE_DECREASE_ALERT) {
                riskAssessment.CaseStatus__c = 'Open';
                riskAssessment.CaseReason__c = 'Major Score Decrease';        
            }
            
            
            riskAssessment.Rating__c = getRating(score);
            riskAssessment.NextReviewDate__c = getNextReviewDate(riskAssessment.Rating__c);
            update  riskAssessment;
            insert rfas;
            
        }
        return toReload;
    }

    public static String getRating(Decimal score) {
        if (score > RiskModelConfig.getInstance().HIGH_RATING_STARTS_WITH){
            return 'High';
        }
        if (score > RiskModelConfig.getInstance().MEDIUM_RATING_STARTS_WITH){
            return 'Medium';
        }
        return 'Low';    	
    }

    public static Date getNextReviewDate(String rating) {
        Date d = Date.today();
        if (rating == 'High') {
           d = d + RiskModelConfig.getInstance().REVIEW_PERIOD_HIGH_RISK;
        }
        if (rating == 'Medium') {
           d = d + RiskModelConfig.getInstance().REVIEW_PERIOD_MEDIUM_RISK;
        }
        if (rating == 'Low') {
           d = d + RiskModelConfig.getInstance().REVIEW_PERIOD_LOW_RISK;
        }
        return d;
    }

    @TestVisible
    private static Decimal getTotalWeight (List<Risk_Factor__c> rfs) {
        Decimal weight = 0;
        for (Risk_Factor__c rf : rfs) {
            weight = weight + rf.Weight__c;
        }
        return weight;
	}
   
    public static List<Risk_Factor__c> getApplicableRiskFactors (String type) {
        List<Risk_Factor__c> rfs = [SELECT Id, 
             Name,
             Source__c,
             Weight__c, 
             Activated__c,
             CustomerType__c
             FROM Risk_Factor__c 
             WHERE Activated__c = true and CustomerType__c includes (:type)];
        return rfs;
    }

    @TestVisible
    private static Risk_Factor_Assignment__c getRiskAssignment(Risk_Factor__c rf, String value, Decimal totalWeight) {
      	Risk_Factor_Value__c rfv;
       	List<Risk_Factor_Value__c> values = [SELECT Value__c, Score__c FROM Risk_Factor_Value__c WHERE Factor__c =:rf.Name AND Value__c  =:value];
        
        if(values.size() > 0){
            rfv = values[0];		
        } else {
            rfv = new Risk_Factor_Value__c();
            rfv.Value__c = 'Not Found - ' + value;
            rfv.Score__c = 100; //maximum Score;
        }
        Risk_Factor_Assignment__c rfa = new Risk_Factor_Assignment__c();
        
        rfa.Name = rf.Name;
        
        rfa.Value__c = rfv.Value__c;
        rfa.Score__c = rfv.Score__c;
       	rfa.Weight__c = rf.Weight__c;
        
        if (RiskModelConfig.getInstance().ALGORITHM_TYPE == 'Dynamic Weight Allocation'){
            rfa.Assigned_Weight__c = 100 * rf.Weight__c/totalWeight; //in percentage points
        } else {
            rfa.Assigned_Weight__c = rf.Weight__c;
        }
        rfa.Assigned_Score__c = rfa.Assigned_Weight__c * rfa.Score__c / 100;
       

        return rfa;
    }

    public static void runDueAssessments () {
        List<Risk_Assessment__c> dueAssessments =  [SELECT Account__c  
            FROM Risk_Assessment__c 
            WHERE NextReviewDate__c < TODAY AND
                  Current__c = true];
        List<String> ids = new List<String>();
        for (Risk_Assessment__c ra : dueAssessments) {
            ids.add(ra.Account__c);
        }
        String qry = QueryBuilder.buildObjectQuery('Account');
        BatchAccountReassessment batch = new BatchAccountReassessment(qry, ids, 'Periodic Review', '' );
    }

    public static void runAcceleratedAssessments (Integer numDays) {
        Date startDate = Date.today().addDays(-Integer.valueOf(numDays));
        List<AccountHistory> dueAssessments = [SELECT AccountId, Field, OldValue, NewValue 
                                        FROM AccountHistory 
                                        WHERE CreatedDate >= :startDate
                                        ORDER BY CreatedDate DESC];
        Set<String> distinctValues = new Set<String>();
        List<String> distinctAccountIds = new List<String>();

        for (AccountHistory a : dueAssessments) {
            if (distinctValues.add(a.AccountId)){
                distinctAccountIds.add(a.AccountId);
            }
        } 
        String qry = QueryBuilder.buildObjectQuery('Account');
        BatchAccountReassessment batch = new BatchAccountReassessment(qry, distinctAccountIds, 'Accelerated Review', 'Changed in the past ' + numDays * 24 + ' hours' );

        Database.executeBatch(batch, 5);
    }

    public static void processRiskModelChanges (String details) {
        if (RiskModelConfig.getInstance().RESCORE_ON_RISK_MODEL_CHANGE){
            String qry = QueryBuilder.buildObjectQuery('Account');
            BatchAccountReassessment batch = new BatchAccountReassessment(qry, 'Risk Model Change', details);

            Database.executeBatch(batch, 5);
        }
    }

    public static Integer calculateMonthDifferenceFromToday(Date fromDate) {
        Date today = Date.today();
        Integer monthsBetween = fromDate.monthsBetween(today);
        return monthsBetween;
    }
}