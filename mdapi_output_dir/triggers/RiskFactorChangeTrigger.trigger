trigger RiskFactorChangeTrigger on Risk_Factor__c (after update) {
	if(Trigger.isUpdate) {
        System.Debug('Risk Model Changed'); 
        Risk_Factor__c rfAll = new Risk_Factor__c(); // This takes all available fields from the required object. 
        Schema.SObjectType objType = rfAll.getSObjectType(); 
        Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.Risk_Factor__c.fields.getMap();
        
        Set<String> triggerFields = new Set<String>();
        triggerFields.add('Activated__c');
        triggerFields.add('CustomerType__c');
        triggerFields.add('Source__c');
        triggerFields.add('Weight__c');
        //TODO: optimize for passing only those accounts that have the triggered fields or values populated
        for(Risk_Factor__c aNew : trigger.new) {
            Risk_Factor__c aOld = trigger.oldMap.get(aNew.Id);
            String message = 'Risk Model field changes included: ';
            Boolean assessmentNeeded = false;
            for (String field : triggerFields) { 
                try { 
                    if(aNew.get(field) != aOld.get(field)) {
                        if (!assessmentNeeded) {
                            assessmentNeeded = true;
                        }               
                        String info = 'For Risk Factor ' + aNew.get('Name') + ': ' + field.removeEnd('__c')+ ': ' + aOld.get(field) + ' -> ' + aNew.get(field) + '; ';
                        message += info;
                        System.Debug(info);
                    } 
                } catch (Exception e) { 
                    System.Debug('Error: ' + e); 
                }
            }
            if (assessmentNeeded){
                RiskModel.processRiskModelChanges(message);
            }            
        }
    }
}