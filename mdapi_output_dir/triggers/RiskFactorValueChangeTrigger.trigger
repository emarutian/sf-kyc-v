trigger RiskFactorValueChangeTrigger on Risk_Factor_Value__c (after update) {
	if(Trigger.isUpdate) {
        System.Debug('Risk Model Changed'); 
        Risk_Factor_Value__c rfAll = new Risk_Factor_Value__c(); // This takes all available fields from the required object. 
        Schema.SObjectType objType = rfAll.getSObjectType(); 
        Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.Risk_Factor_Value__c.fields.getMap();
        
        Set<String> triggerFields = new Set<String>();
        triggerFields.add('Is_Range__c');
        triggerFields.add('Max__c');
        triggerFields.add('Min__c');
        triggerFields.add('Score__c');
        triggerFields.add('Value__c');       
        //TODO: optimize for passing only those accounts that have the triggered fields or values populated 
        for(Risk_Factor_Value__c aNew : trigger.new) {
            Risk_Factor_Value__c aOld = trigger.oldMap.get(aNew.Id);
            String message = 'Risk Model field changes included: ';
            Boolean assessmentNeeded = false;
            for (String field : triggerFields) { 
                try { 
                    if(aNew.get(field) != aOld.get(field)) {
                        if (!assessmentNeeded) {
                            assessmentNeeded = true;
                        }               
                        String info ='For Risk Factor Value: ' + aNew.get('Factor__c') + ': ' + field.removeEnd('__c')+ ': ' + aOld.get(field) + ' -> ' + aNew.get(field) + '; ';
                        message += info;
                        System.Debug(info);
                    } 
                } catch (Exception e) { 
                    System.Debug('Error: ' + e); 
                }
            }
            if (assessmentNeeded){
                RiskModel.processRiskModelChanges(message);
            }            
        }
    }
}