trigger AcceleratedReviewTrigger on Account (after update) {
    if(Trigger.isUpdate) {
        System.Debug('Update triggered'); 
        Account a = new Account(); // This takes all available fields from the required object. 
        Schema.SObjectType objType = a.getSObjectType(); 
        Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.Account.fields.getMap();
        
        for(Account aNew : trigger.new) {
            List<Risk_Factor__c> rfs = RiskModel.getApplicableRiskFactors(aNew.id);
            Set<String> rfSet = new Set<String>();
            for (Risk_Factor__c rf: rfs) {
                rfSet.add(rf.Source__c.toLowerCase());   
            }
            for (String v : rfSet){
                 System.Debug('RF Field: ' + v);
            }
            Account aOld = trigger.oldMap.get(aNew.Id);
            String message = 'Risk sensitive field changes for Account fields: ';
            Boolean acceleratedReviewNeeded = false;
            for (String field : mapFields.keyset()) { 
                try {
                    if(aNew.get(field) != aOld.get(field) && rfSet.contains(field)) {
                        if (!acceleratedReviewNeeded) {
                            acceleratedReviewNeeded = true;
                        }               
                        String info = field.removeEnd('__c')+ ': ' + aOld.get(field) + ' -> ' + aNew.get(field) + '; ';
                        message += info;
                        System.Debug(info);
                    } 
                } catch (Exception e) { 
                    System.Debug('Error: ' + e); 
                }
            }
            if (acceleratedReviewNeeded){
                RiskModel.assessAccount(aNew, 'Accelerated Review', message);
            }            
        }
    }
}