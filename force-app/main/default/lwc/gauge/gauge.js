/* eslint-disable no-console */
//https://bernii.github.io/gauge.js/
import { LightningElement, wire, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadScript } from 'lightning/platformResourceLoader';
import KYC from '@salesforce/resourceUrl/kyc';
import getRiskAssessment from '@salesforce/apex/RiskModelController.getRiskAssessment';


export default class gauge extends LightningElement {

    @api recordId;
    @track score;
    @track error;
    @track rating;
    @track nextReviewDate;
    
    @wire(getRiskAssessment, { id: '$recordId' }) 
    riskAssessment({ error, data }) {
        if (data) {
            // console.log('loggin data');
            // console.log(data.Rating__c);
            // console.log(data.Score__c);

            this.score = Math.round(data.Score__c);
            this.rating = data.Rating__c;
            this.nextReviewDate = data.NextReviewDate__c;
           // this.assessment = data;
            this.initializeGauge()
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.record = undefined;
        }
    }

    initialized = false;

    renderedCallback() {
        if (this.initialized) {
            return;
        }
        this.initialized = true;

        Promise.all([
            loadScript(this, KYC + '/gauge.min.js'),
        ])
            .then(() => {
                //this.initializeGauge();
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error loading Gauge',
                        message: error.message,
                        variant: 'error'
                    })
                );
            });
    }

    initializeGauge() {
        var opts = {
            lines: 12,
            angle: 0.15,
            lineWidth: 0.44,
            pointer: {
              length: 0.9,
              strokeWidth: 0.035,
              color: '#000000'
            },
            limitMax: 'false', 
            percentColors: [[0.0, "#a9d70b" ], [0.50, "#f9c802"], [1.0, "#ff0000"]], // !!!!
            strokeColor: '#E0E0E0',
            generateGradient: true
          };

        // var opts = {
        //     lines: 12,
        //     angle: 0,
        //     lineWidth: 0.44,
        //     pointer: {
        //       length: 0.0,
        //       strokeWidth: 0.0,
        //       color: '#000000'
        //     },
        //     limitMax: 'false', 
        //     percentColors: [[0.0, "#a9d70b" ], [0.50, "#f9c802"], [1.0, "#ff0000"]], // !!!!
        //     strokeColor: '#E0E0E0',
        //     generateGradient: true
        //   };
          var target = this.template.querySelector('canvas.gauge');
          var gauge = new Gauge(target).setOptions(opts);
          gauge.maxValue = 100;
          gauge.animationSpeed = 32;
          // eslint-disable-next-line no-console
          //console.log('this.riskAssessment: ')
          // eslint-disable-next-line no-console
          gauge.set(this.score);
          /*
        var opts = {
            angle: 0.0, // The span of the gauge arc
            lineWidth: 0.44, // The line thickness
            radiusScale: 1, // Relative radius
            pointer: {
              length: 0.6, // // Relative to gauge radius
              strokeWidth: 0.035, // The thickness
              color: '#000000' // Fill color
            },
            staticZones: [
                {strokeStyle: "green", min: 0, max: 40, height: 1},
                {strokeStyle: "yellow", min: 40, max: 80, height: 1},
                {strokeStyle: "red", min: 80, max: 100, height: 1}
              ],
              
            limitMax: false,     // If false, max value increases automatically if value > maxValue
            limitMin: false,     // If true, the min value of the gauge will be fixed
            colorStart: '#6FADCF',   // Colors
            colorStop: '#8FC0DA',    // just experiment with them
            strokeColor: '#E0E0E0',  // to see which ones work best for you
            generateGradient: true,
            highDpiSupport: true,     // High resolution support
            
          };
          var target = this.template.querySelector('canvas.canvas-preview'); // your canvas element
          var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
          gauge.maxValue = 100; // set max gauge value
          gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
          gauge.animationSpeed = 32; // set animation speed (32 is default value)
          gauge.set(80); // set actual value
          */
    }
}