@isTest (SeeAllData=true)
private with sharing class BatchAccountReassessmentTest {

    static testmethod void test() {        
        List<Account> accounts = new List<Account>();
        // insert 10 accounts
        for (Integer i=0;i<10;i++) {
            accounts.add(new Account(name='Account '+i, 
                billingcity='New York Test', billingcountry='USA'));
        }
        insert accounts;
        // find the account just inserted. add contact for each
        List<String> ids = new List<String>();
        for (Account account : accounts) {
            System.Debug('>> Account id: ' + account.id);
           ids.add(account.id);
        }
        BatchAccountReassessment batch = new BatchAccountReassessment(
                                            QueryBuilder.buildObjectQuery('Account'), ids, 'Risk Model Change', 'none');
        Id batchId = Database.executeBatch(batch);
        
        List<Risk_Assessment__c> ras = [select id from Risk_Assessment__c where Account__c in :ids];
        // after the testing stops, assert records were updated properly
        System.assertEquals(accounts.size(), ras.size());
    }
    
}