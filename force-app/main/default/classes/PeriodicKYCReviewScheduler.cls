global class PeriodicKYCReviewScheduler implements Schedulable {
    global void execute(SchedulableContext ctx) {
		RiskModel.runDueAssessments();
    }
}