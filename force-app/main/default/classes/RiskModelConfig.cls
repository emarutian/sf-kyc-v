public class RiskModelConfig {
    
    // Configuration variables
    public String  Id {get;private set;}
    public Decimal IDV_DEFAULT_SCORE {get;private set;}
    public Integer ACCOUNT_OPEN_RANGE {get;private set;}
    public Integer DEPLOYMENT_INITIATION_RANGE {get;private set;}
    public Boolean ASSESS_RELATED_PARTIES {get;private set;}
    public Boolean ASSESS_CONTROLLING_ROLES_ONLY {get;private set;}
    public Integer REVIEW_PERIOD_LOW_RISK {get;private set;}
    public Integer REVIEW_PERIOD_MEDIUM_RISK {get;private set;}
    public Integer REVIEW_PERIOD_HIGH_RISK {get;private set;}
    public Boolean RULE_BASED_ASSESSMENT {get;private set;}
    public Boolean USER_REVIEW_FLAG {get;private set;}
    public Date    LEGACY_CUSTOMER_START_DATE {get;private set;}
    public Boolean RESCORE_ON_RISK_MODEL_CHANGE {get;private set;}
    public Integer HIGH_RATING_STARTS_WITH {get;private set;}
    public Integer MEDIUM_RATING_STARTS_WITH {get;private set;}
    public Integer HIGH_SCORE_ALERT {get;private set;}
	public Integer MAJOR_SCORE_INCREASE_ALERT {get;private set;}
    public Integer MAJOR_SCORE_DECREASE_ALERT {get;private set;}
    public String  ALGORITHM_TYPE {get;private set;}


    
    
    // private static variable referencing the class
    private static RiskModelConfig instance = null;
    
    // The constructor is private and initializes the id of the record type
    private RiskModelConfig(){
        Risk_Model_Configuration__c activeConfig;
        List<Risk_Model_Configuration__c> configs = [SELECT IDVDefaultScore__c,
                                                            AccountOpenRange__c,
                                                            DeploymentInitiationRange__c,
                                                            AssessRelatedParties__c,
                                                            AssessControllingRolesOnly__c,
                                                            ReviewPeriodLowRisk__c,
                                                            ReviewPeriodMediumRisk__c,
                                                            ReviewPeriodHighRisk__c,
                                                            RuleBasedAssessment__c,
                                                            UserReviewFlag__c,
                                                            LegacyCustomerStartDate__c,
                                                            RescoreOnRiskModelChange__c,
                                                     		HighRatingStartsWith__c,
                                                     		MediumRatingStartsWith__c,
                                                     		MajorScoreIncreaseAlert__c,
                                                     		MajorScoreDecreaseAlert__c,
                                                     		HighScoreAlert__c,
                                                     		AlgorithmType__c,
                                                     		Name
                                                     
                                                    	FROM Risk_Model_Configuration__c WHERE Activated__c = true]; 
        if (configs.size() > 0){
            activeConfig = configs[0];							
            Id								= activeConfig.Id;
            IDV_DEFAULT_SCORE 				= activeConfig.IDVDefaultScore__c;
            ACCOUNT_OPEN_RANGE 				= (Integer)activeConfig.AccountOpenRange__c;
            DEPLOYMENT_INITIATION_RANGE 	= (Integer)activeConfig.DeploymentInitiationRange__c;
            ASSESS_RELATED_PARTIES 			= activeConfig.AssessRelatedParties__c;
            ASSESS_CONTROLLING_ROLES_ONLY 	= activeConfig.AssessControllingRolesOnly__c;
            REVIEW_PERIOD_LOW_RISK 			= (Integer)activeConfig.ReviewPeriodLowRisk__c;
            REVIEW_PERIOD_MEDIUM_RISK 		= (Integer)activeConfig.ReviewPeriodMediumRisk__c;
            REVIEW_PERIOD_HIGH_RISK 		= (Integer)activeConfig.ReviewPeriodHighRisk__c;
            RULE_BASED_ASSESSMENT 			= activeConfig.RuleBasedAssessment__c;
            USER_REVIEW_FLAG 				= activeConfig.UserReviewFlag__c;
            LEGACY_CUSTOMER_START_DATE 		= activeConfig.LegacyCustomerStartDate__c;
            RESCORE_ON_RISK_MODEL_CHANGE 	= activeConfig.RescoreOnRiskModelChange__c;
            HIGH_RATING_STARTS_WITH 		= (Integer)activeConfig.HighRatingStartsWith__c;
            MEDIUM_RATING_STARTS_WITH 		= (Integer)activeConfig.MediumRatingStartsWith__c;
         	HIGH_SCORE_ALERT		 		= (Integer)activeConfig.HighScoreAlert__c;
            MAJOR_SCORE_INCREASE_ALERT		= (Integer)activeConfig.MajorScoreIncreaseAlert__c;
            MAJOR_SCORE_DECREASE_ALERT		= (Integer)activeConfig.MajorScoreDecreaseAlert__c;
            ALGORITHM_TYPE					= activeConfig.AlgorithmType__c;
            
        } else {            
            activeConfig = new Risk_Model_Configuration__c();
            activeConfig.Name = 						'Created by System on ' + Date.today();
            activeConfig.Activated__c = 				true;
            activeConfig.IDVDefaultScore__c = 			100;
            activeConfig.AccountOpenRange__c = 			7;
            activeConfig.DeploymentInitiationRange__c = 365;
            activeConfig.AssessRelatedParties__c = 		true;
            activeConfig.AssessControllingRolesOnly__c= true;
            activeConfig.ReviewPeriodLowRisk__c = 		365 * 3;
            activeConfig.ReviewPeriodMediumRisk__c = 	365 * 2;
            activeConfig.ReviewPeriodHighRisk__c = 		356;
            activeConfig.RuleBasedAssessment__c = 		true;
            activeConfig.UserReviewFlag__c 	= 			true;
            activeConfig.LegacyCustomerStartDate__c  = 	Date.newInstance(2005, 1, 1);
            activeConfig.RescoreOnRiskModelChange__c = 	false;
            activeConfig.HighRatingStartsWith__c    = 	60;
            activeConfig.MediumRatingStartsWith__c  = 	40;
            activeConfig.MajorScoreIncreaseAlert__c =	40;
            activeConfig.MajorScoreDecreaseAlert__c =	40;
            activeConfig.HighScoreAlert__c			=	60;
           	activeConfig.AlgorithmType__c			=	'Dynamic Weight Allocation';
            
            
            insert activeConfig;
        }
    }
    // a static method that returns the instance of the record type
    public static RiskModelConfig getInstance(){
        // lazy load the record type - only initialize if it doesn't already exist
        if(instance == null) instance = new RiskModelConfig();
        return instance;
    }
}