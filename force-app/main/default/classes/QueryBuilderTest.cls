@isTest (SeeAllData=true)
public class QueryBuilderTest {
    static testMethod void validateQueryBuilder () {
        Account expected = new Account(
            Name='Test Account',
            CountryOfOperation__c = 'Angola');
        
        insert expected;
        String query = QueryBuilder.buildObjectQuery('Account', 'WHERE id = \'' + expected.id + '\'');
        Account actual = (Account)Database.query(query)[0];
        System.assertEquals(actual.get('name'), expected.get('name'));
        System.assertEquals(actual.get('CountryOfOperation__c'), expected.get('CountryOfOperation__c'));
    }
    static testMethod void validateGetObjectId () {
        Account expected = new Account(
            Name='Test Account',
            CountryOfOperation__c = 'Angola');
        
        insert expected;
        Account actual = (Account)QueryBuilder.getObjectById('Account', expected.id);
        System.assertEquals(actual.get('name'), expected.get('name'));
        System.assertEquals(actual.id, expected.id);
        System.assertEquals(actual.get('CountryOfOperation__c'), expected.get('CountryOfOperation__c'));

    }
}