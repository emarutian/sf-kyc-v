global class BatchAccountReassessment implements Database.Batchable<sObject>{
   global String query;
   global String reason;
   global String type;
   global String filter;

   global BatchAccountReassessment(String query, String type, String reason){
      this.query=query; 
      this.reason=reason; 
      this.type=type;
      System.Debug('==> query is: ' + this.query);
   }
   global BatchAccountReassessment(String query, String[] filter, String type, String reason){
      if(query.toLowerCase().contains('where')) {
         this.query=query + ' and id in: filter'; 
      } else {
         this.query=query + ' where id in: filter'; 
      }
             
      this.reason=reason; 
      this.type=type;
      System.Debug('====> query is: ' + this.query);
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      System.assertNotEquals(null, this.query, 'The query should be defined');
      return Database.getQueryLocator(this.query);
   }

   global void execute(Database.BatchableContext BC, List<Account> accounts){
      System.Debug('====> Accounts in batch are: ' + accounts.size());
      for(Account a : accounts){
         RiskModel.assessAccount(a, type, reason);
      }
   }

   global void finish(Database.BatchableContext BC){

   }

}