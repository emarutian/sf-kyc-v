global with sharing class MassDeleteJob implements Database.Batchable<sObject>{
   global String query;

   global MassDeleteJob(String query){
      this.query=query; 
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      System.assertNotEquals(null, this.query, 'The query should be defined');
      return Database.getQueryLocator(this.query);
   }

   global void execute(Database.BatchableContext BC, List<SObject> objects){
      delete objects;
   }

   global void finish(Database.BatchableContext BC){
       System.Debug('Deletion of records completed');
   }

}