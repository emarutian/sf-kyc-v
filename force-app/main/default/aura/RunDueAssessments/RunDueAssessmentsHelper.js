({
	runDueAssessments : function(component, event, helper) {
		var action = component.get("c.runDueAssessments");
        action.setCallback(this, function (a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                var isCreated = a.getReturnValue();
                component.set("v.isCreated",isCreated);
                component.set("v.showSpinner",false);
                $A.get('e.force:refreshView').fire();
            } else {
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " +
                            errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	}
})